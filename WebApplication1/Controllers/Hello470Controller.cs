﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class Hello470Controller : Controller
    {
        // GET: Hello470
        public ActionResult Index()
        {
            return View();
        }

        public string Welcome()
        {
            return "Welcome method";
        }
    }
}