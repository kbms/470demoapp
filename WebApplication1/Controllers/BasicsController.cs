﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class BasicsController : Controller
    {
        // GET: Basics
        public ActionResult Index(string query,string name)
        {
            ViewBag.Query = query;
            ViewBag.Name = name;
            return View();
        }
    }
}